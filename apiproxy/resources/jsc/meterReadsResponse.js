var response = context.getVariable("soapResponse.body");
if (typeof response != 'undefined') {
  var responseBody = JSON.parse(response);
  var responseCode = responseBody.statusCode;
  var deviceUtilId = responseBody.meterReadResult.deviceUtilID;
  var deviceMACId = responseBody.meterReadResult.deviceMacID;
  var readTime = responseBody.meterReadResult.readTime;
  var meterRegisterReadTier = responseBody.meterReadResult.registerReads.registerRead.tier;
  var responseObject = {};
  var meter = {};
  var finalObject = {};
  if (responseCode === 'Success') {
    context.setVariable('response.status.code', 200);

    var correlationId = context.getVariable("correlationId");
    // map correlationId
    if (responseBody.requestID != "NULL") {
      responseObject.correlationId = correlationId;
    } else {
      responseObject.correlationId = "{requestId}";
    }
    // map meters
    if (responseBody.statusCode != "NULL") {
      var tiers = [];

      // map deviceUtilId
      if (request.deviceUtilId != "NULL") {
        var deviceUtilId = context.getVariable("request.queryparam.deviceUtilId");
        meter.deviceUtilId = deviceUtilId;
      } else {
        meter.deviceUtilId = "";
      }

      // map deviceMacId
      if (deviceMACId != "NULL") {
        meter.deviceMACId = deviceMACId;
      } else {
        meter.deviceMACId = "";
      }

      // map timeStamp
      if (readTime != "NULL") {
        meter.timeStamp = readTime.toString().substring(0, 23);
      } else {
        meter.timeStamp = "";
      }

      meter.registers = [];
      
      for (var i = 0; i < meterRegisterReadTier.length; i++) {

        var tierNumber = meterRegisterReadTier[i].tierNumber.toString();
        var meterRegisterReadTiersRegisters = responseBody.meterReadResult.registerReads.registerRead.tier[i].Register;
        if (!Array.isArray(meterRegisterReadTiersRegisters)) {
          meterRegisterReadTiersRegisters = Array(responseBody.meterReadResult.registerReads.registerRead.tier[i].Register);
        }
        if (meterRegisterReadTiersRegisters != null) {
          print(i);
          for (var j = 0; j < meterRegisterReadTiersRegisters.length; j++) {
            print(j);
            var UOM = meterRegisterReadTiersRegisters[j].SummationUOM;
            print(UOM);
            if (UOM.includes('kWh')) {
              var register = {};
              register.tier = meterRegisterReadTier[i].tierNumber.toString();
              register.channel = meterRegisterReadTiersRegisters[j].SummationIEEExportChannelID.toString();
              register.value = meterRegisterReadTiersRegisters[j].Summation.toFixed(4);
              register.unit = meterRegisterReadTiersRegisters[j].SummationUOM.toString();
              register.unitDescription = meterRegisterReadTiersRegisters[j].SummationUOMDescription.toString();
              meter.registers.push(register);
            }
          }
        }
      }
      responseObject.meter = meter;
      context.setVariable("finalResponse", JSON.stringify(responseObject));
    }
  }
}