<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" 
xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" 
xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xslt="http://xml.apache.org/xslt" 
xmlns:v1="http://services.xcelenergy.com/services/Customer/MeterDataMgmt/AMIOnDemandDevicePing/v1/" 
xmlns:v2="http://services.xcelenergy.com/schema/EAI/Common/ESBHeader/v2/" 
soap:mustUnderstand="true">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" xslt:indent-amount="2"/>
	<xsl:strip-space elements="*"/>
	<xsl:param name="usernameTokenId" select="''"/>
	<xsl:param name="username" select="''"/>
	<xsl:param name="password" select="''"/>
	<xsl:param name="requestId" select="''"/>
	<xsl:param name="deviceUtilId" select="''"/>

	<xsl:template match="/soap:Envelope">
		<soap:Envelope>
			<soap:Header>
				<xsl:choose>
					<xsl:when test="soap:Header">
						<xsl:apply-templates select="soap:Header"/>
					</xsl:when>
					<xsl:otherwise>
						<wsse:Security>
							<xsl:call-template name="injectUsernameToken"/>
						</wsse:Security>
					</xsl:otherwise>
				</xsl:choose>
			</soap:Header>
			<soap:Body>
				<xsl:apply-templates select="soap:Body"/>
			</soap:Body>
		</soap:Envelope>
	</xsl:template>
	<xsl:template match="soap:Body">
      <v1:pingDevice>
          <esbHeader>
            <v2:transactionID><xsl:value-of select="$requestId"/></v2:transactionID>
            <v2:creationTime>2020-09-30T13:20:59.648-06:00</v2:creationTime>
            <v2:environmentType>TEST</v2:environmentType>
            <v2:clientID/>
            <v2:systemID/>
            <v2:applicationID>CXT</v2:applicationID>
            <v2:userID>APIGEE</v2:userID>
         </esbHeader>
         <deviceUtilID><xsl:value-of select="$deviceUtilId"/></deviceUtilID>
      </v1:pingDevice>
	</xsl:template>
	<xsl:template match="soap:Header">
		<xsl:choose>
			<xsl:when test='wsse:Security'>
				<xsl:apply-templates select="*"/>
			</xsl:when>
			<xsl:otherwise>
				<wsse:Security>
					<xsl:call-template name="injectUsernameToken"/>
				</wsse:Security>
				<xsl:copy-of select="*"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="soap:Header/wsse:Security/wsse:UsernameToken"/>
	<xsl:template name='injectUsernameToken'>
		<wsse:UsernameToken>
			<xsl:attribute name="wsu:Id"><xsl:value-of select="$usernameTokenId"/></xsl:attribute>
			<wsse:Username><xsl:value-of select="$username"/></wsse:Username>
			<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"><xsl:value-of select="$password"/></wsse:Password>
			<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">d9XdEP7j8dFot7RIpjgt7A==</wsse:Nonce>
            <wsu:Created>2019-08-18T17:12:36.944Z</wsu:Created>
		</wsse:UsernameToken>
	</xsl:template>
</xsl:stylesheet>